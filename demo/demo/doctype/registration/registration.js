// Copyright (c) 2021, new and contributors
// For license information, please see license.txt

frappe.ui.form.on('Registration', {
	 before_save: function(frm) {
		var x;
		frappe.db.get_doc('Lab Id Master', 'f6f1b62c6a')
    			.then(doc => {
        	x = doc.id;
		frappe.model.set_value("Lab Id Master","f6f1b62c6a","id",cint(x)+1);
		frappe.db.set_value("Lab Id Master","f6f1b62c6a","id",cint(x)+1);
		cur_frm.set_value("lab_id",x);
    			})
		cur_frm.set_value("reg_by",frappe.session.user);	
	}
});
